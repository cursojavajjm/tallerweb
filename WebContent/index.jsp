<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import='java.util.Date'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bienvenido a TallerWeb</title>
</head>
<body>
	<center>
		<img alt="TallerWeb"  src="${pageContext.request.contextPath}/images/tallerweb.png" />
		<h1>Conectarse a TallerWeb</h1>
		<p>Escriba su nombre de usuario y contraseņa</p>
		<form action="/TallerWeb/control" method="POST">
			<p><input type="text" name="usuario" placeholder="nombre de usuario"/></p>
			<p><input type="password" name="password" placeholder="contraseņa"/></p>
			<p><input type="submit" name="Conectarse"/></p>		
		</form>
		<p>Actualizado a fecha <%= new Date() %></p>
	</center>
</body>
</html>