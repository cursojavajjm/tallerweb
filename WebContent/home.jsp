<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import='com.tallerweb.modelos.Cliente'%>
<%@ page import='java.util.Collection'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 16px;
    text-decoration: none;
}

li a:hover {
    background-color: #111111;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TallerWeb - Panel de administracion</title>
</head>
<body>
	<center>
		<h1>TallerWeb - Panel de Administración</h1>
		<div>
			<ul>
			  <li><a href="">Home</a></li>
			  <li><a href="/TallerWeb/clientes">Clientes</a></li>
			  <li><a href="/TallerWeb/mecanicos">Mecanicos</a></li>
			  <li><a href="/TallerWeb/facturas">Facturas</a></li>
			</ul>
		</div>
		<div>
			<table>
				<tr>
					<th>DNI</th>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Direccion</th>
					<th>Telefono</th>
				</tr>
			<%! Collection<Cliente> clientes; %>
			<% clientes = (Collection<Cliente>) request.getAttribute("clientes"); %>
			<% for(Cliente c : clientes) { %>
				<tr>
					<td><a href="/TallerWeb/cliente?dni=<%= c.getDni() %>"><%= c.getDni() %></a></td>
					<td><%= c.getNombre() %></td>
					<td><%= c.getApellidos() %></td>
					<td><%= c.getDireccion() %></td>
					<td><%= c.getTelefono() %></td>
				</tr>
			<% } %>
			</table>
		</div>
	</center>
</body>
</html>