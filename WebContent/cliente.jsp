<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import='com.tallerweb.modelos.Vehiculo'%>
<%@ page import='com.tallerweb.modelos.Cliente'%>
<%@ page import='java.util.Collection'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 16px;
    text-decoration: none;
}

li a:hover {
    background-color: #111111;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TallerWeb - Panel de administracion</title>
</head>
<body>
	<center>
		<h1>TallerWeb - Panel de Administración</h1>
		<div>
			<ul>
			  <li><a href="">Home</a></li>
			  <li><a href="/TallerWeb/clientes">Clientes</a></li>
			  <li><a href="/TallerWeb/mecanicos">Mecanicos</a></li>
			  <li><a href="/TallerWeb/facturas">Facturas</a></li>
			</ul>
		</div>
		<%! Cliente c; %>
		<% c = (Cliente) request.getAttribute("cliente"); %>
		<div>
			<h2>Datos del Cliente</h2>
			<p>DNI: <%= c.getNombre() %></p>
			<p>Nombre: <%= c.getNombre() %></p>
			<p>Apellidos: <%= c.getApellidos() %></p>
			<p>Direccion: <%= c.getDireccion() %></p>
			<p>Telefono: <%= c.getTelefono() %></p>
		</div>
		<div>
			<h2>Vehiculos del cliente</h2>
			<table>
				<tr>
					<th>Matricula</th>
					<th>Modelo</th>
					<th>Color</th>
				</tr>
			<%! Collection<Vehiculo> vehiculos; %>
			<% vehiculos = (Collection<Vehiculo>) request.getAttribute("vehiculos"); %>
			<% for(Vehiculo v : vehiculos) { %>
				<tr>
					<td><%= v.getMatricula() %></td>
					<td><%= v.getModelo() %></td>
					<td><%= v.getColor() %></td>
				</tr>
			<% } %>
			</table>
		</div>
	</center>
</body>
</html>