package com.tallerweb.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.tallerweb.modelos.Repuesto;
import com.tallerweb.servicios.IRepuestoDAO;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class RepuestoDbDAO implements IRepuestoDAO {
	
	private Connection conn;
	
	public RepuestoDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean guardar(Repuesto r) {
		throw new NotImplementedException();
	}

	@Override
	public Repuesto recuperar(int id) {
		String sql = "SELECT * FROM repuestos WHERE idRepuesto = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setInt(1, id);
				rs = st.executeQuery();
				if(rs.next()) {
					int idRepuesto = rs.getInt("idRepuesto");
					String nombre = rs.getString("nombre");
					int precioUnidad = rs.getInt("precioUnidad");
					
					Repuesto r = new Repuesto(idRepuesto, nombre, precioUnidad);
					return r;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

	@Override
	public List<Repuesto> buscarPorNombre(String nombre) {
		String sql = "SELECT * FROM repuestos WHERE nombre = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		List<Repuesto> repuestos = new ArrayList<Repuesto>();
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setString(1, nombre);
				rs = st.executeQuery();
				while(rs.next()) {
					int idRepuesto = rs.getInt("idRepuesto");
					String nombreRep = rs.getString("nombre");
					int precioUnidad = rs.getInt("precioUnidad");
					
					Repuesto r = new Repuesto(idRepuesto, nombreRep, precioUnidad);
					repuestos.add(r);
				}
				return repuestos;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

}
