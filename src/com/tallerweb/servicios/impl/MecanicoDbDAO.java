package com.tallerweb.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.tallerweb.modelos.Mecanico;
import com.tallerweb.servicios.IMecanicoDAO;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class MecanicoDbDAO implements IMecanicoDAO {
	
	private Connection conn;
	
	public MecanicoDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean guardar(Mecanico c) {
		throw new NotImplementedException();
	}

	@Override
	public Mecanico recuperar(int id) {
		throw new NotImplementedException();
	}

	@Override
	public Mecanico buscarMecanicoLibre() {
		String sql = "SELECT * FROM mecanicos WHERE libre = true LIMIT 1";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				rs = st.executeQuery(sql);
				if(rs.next()) {
					int num = rs.getInt("numEmpleado");
					String nombre = rs.getString("nombre");
					boolean libre = rs.getBoolean("libre");
					
					Mecanico m = new Mecanico(num, nombre);
					return m;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

}
