package com.tallerweb.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.tallerweb.modelos.Reparacion;
import com.tallerweb.servicios.IReparacionDAO;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ReparacionDbDAO implements IReparacionDAO {
	
	private Connection conn;
	
	public ReparacionDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean guardar(Reparacion r) {
		throw new NotImplementedException();
	}

	@Override
	public Reparacion recuperar(int id) {
		String sql = "SELECT * FROM reparaciones WHERE idReparacion = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setInt(1, id);
				rs = st.executeQuery();
				if(rs.next()) {
					int idReparacion = rs.getInt("idReparacion");
					int hojaParte = rs.getInt("hojaParte");
					int mecanico = rs.getInt("mecanico");
					int repuesto = rs.getInt("repuesto");
					int precioManoObra = rs.getInt("precioManoObra");
					int cantidad = rs.getInt("cantidad");
					
					Reparacion r = new Reparacion(hojaParte, mecanico, repuesto, cantidad, precioManoObra);
					return r;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

	@Override
	public List<Reparacion> buscarPorParte(int idHojaParte) {
		String sql = "SELECT * FROM reparaciones WHERE hojaParte = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		List<Reparacion> reparaciones = new ArrayList<Reparacion>();
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setInt(1, idHojaParte);
				rs = st.executeQuery();
				while(rs.next()) {
					int idReparacion = rs.getInt("idReparacion");
					int hojaParte = rs.getInt("hojaParte");
					int mecanico = rs.getInt("mecanico");
					int repuesto = rs.getInt("repuesto");
					int precioManoObra = rs.getInt("precioManoObra");
					int cantidad = rs.getInt("cantidad");
					
					Reparacion r = new Reparacion(idReparacion, hojaParte, mecanico, repuesto, cantidad, precioManoObra);
					reparaciones.add(r);
				}
				return reparaciones;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

}
