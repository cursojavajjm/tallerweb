package com.tallerweb.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.tallerweb.modelos.Cliente;
import com.tallerweb.modelos.Usuario;
import com.tallerweb.servicios.IUsuarioDAO;

public class UsuarioDbDAO implements IUsuarioDAO {
	
	private Connection conn;
	
	public UsuarioDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Usuario recuperar(String nombre, String password) {
		String sql = "SELECT * FROM usuarios WHERE usuario = ? AND password = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setString(1, nombre);
				st.setString(2, password);
				rs = st.executeQuery();
				if(rs.next()) {
					String usuario = rs.getString("usuario");
					String pwd = rs.getString("password");
					boolean isAdmin = rs.getBoolean("admin");
					
					Usuario u = new Usuario(usuario, pwd, isAdmin);
					return u;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

	@Override
	public boolean isAdmin(String nombre) {
		String sql = "SELECT isAdmin FROM usuarios WHERE usuario = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setString(1, nombre);
				rs = st.executeQuery();
				if(rs.next()) {
					boolean isAdmin = rs.getBoolean(1);
					return isAdmin;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return false;
	}

}
