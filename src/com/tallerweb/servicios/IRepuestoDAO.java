package com.tallerweb.servicios;

import java.util.List;

import com.tallerweb.modelos.Reparacion;
import com.tallerweb.modelos.Repuesto;

public interface IRepuestoDAO {
	
	boolean guardar(Repuesto r);
	Repuesto recuperar(int id);
	
	List<Repuesto> buscarPorNombre(String nombre);

}
