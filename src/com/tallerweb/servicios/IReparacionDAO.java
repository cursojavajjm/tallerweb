package com.tallerweb.servicios;

import java.util.List;

import com.tallerweb.modelos.Reparacion;

public interface IReparacionDAO {

	boolean guardar(Reparacion r);
	Reparacion recuperar(int id);
	
	List<Reparacion> buscarPorParte(int idHojaParte);

}
