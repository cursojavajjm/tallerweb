package com.tallerweb.servicios;

import com.tallerweb.modelos.Usuario;

public interface IUsuarioDAO {
	
	Usuario recuperar(String nombre, String password);
	
	boolean isAdmin(String nombre);

}
