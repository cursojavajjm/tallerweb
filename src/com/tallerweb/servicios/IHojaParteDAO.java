package com.tallerweb.servicios;

import com.tallerweb.modelos.HojaParte;
import com.tallerweb.modelos.Reparacion;

public interface IHojaParteDAO {
	
	boolean guardar(HojaParte hoja);
	
	HojaParte recuperar(int id);
	
	boolean anadirMecanico(int idHojaParte, int idMecanico, String evaluacion);
	
	boolean anadirReparacion(Reparacion r);
	

}
