package com.tallerweb.servicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
	
	private Connection conn;
	
	public DatabaseManager(String bdUrl, String bdUser, String bdPwd) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.conn = DriverManager.getConnection(bdUrl, bdUser, bdPwd);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Connection obtenerConexion() {
		return this.conn;
	}
	
	public void cerrarConexion() {
		if (this.conn != null)
			try {
				this.conn.close();
			} catch (Exception e) { /**/ }
	}

}
