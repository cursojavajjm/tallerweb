package com.tallerweb.servicios;

import java.util.Collection;

import com.tallerweb.modelos.Vehiculo;

public interface IVehiculoDAO {
	
	boolean guardar(Vehiculo v);
	Vehiculo recuperar(String matricula);
	
	Collection<Vehiculo> recuperarPorCliente(String dni);

}
