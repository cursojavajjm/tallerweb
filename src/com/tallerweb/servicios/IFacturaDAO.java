package com.tallerweb.servicios;

import com.tallerweb.modelos.Factura;

public interface IFacturaDAO {
	
	boolean guardar(Factura factura);
	
	boolean recuperar(int id);
	
	Factura generarFactura(int idHojaParte);

}
