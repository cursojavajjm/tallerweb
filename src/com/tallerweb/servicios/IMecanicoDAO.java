package com.tallerweb.servicios;

import com.tallerweb.modelos.Mecanico;

public interface IMecanicoDAO {
	
	boolean guardar(Mecanico c);
	Mecanico recuperar(int id);
	
	Mecanico buscarMecanicoLibre();

}
