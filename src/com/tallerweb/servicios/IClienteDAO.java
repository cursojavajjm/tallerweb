package com.tallerweb.servicios;

import java.util.Collection;

import com.tallerweb.modelos.Cliente;

public interface IClienteDAO {
	
	boolean guardar(Cliente c);
	Cliente recuperar(String dni);
	Cliente buscarPorNombre(String nombre);
	Cliente buscarPorTelefono(int telefono);
	Collection<Cliente> recuperarTodos();
	
}
