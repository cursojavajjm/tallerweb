package com.tallerweb.web.listeners;

import java.sql.Connection;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.tallerweb.servicios.DatabaseManager;
import com.tallerweb.servicios.IClienteDAO;
import com.tallerweb.servicios.IFacturaDAO;
import com.tallerweb.servicios.IHojaParteDAO;
import com.tallerweb.servicios.IMecanicoDAO;
import com.tallerweb.servicios.IReparacionDAO;
import com.tallerweb.servicios.IRepuestoDAO;
import com.tallerweb.servicios.IUsuarioDAO;
import com.tallerweb.servicios.IVehiculoDAO;
import com.tallerweb.servicios.impl.ClienteDbDAO;
import com.tallerweb.servicios.impl.FacturaDbDAO;
import com.tallerweb.servicios.impl.HojaParteDbDAO;
import com.tallerweb.servicios.impl.MecanicoDbDAO;
import com.tallerweb.servicios.impl.ReparacionDbDAO;
import com.tallerweb.servicios.impl.RepuestoDbDAO;
import com.tallerweb.servicios.impl.UsuarioDbDAO;
import com.tallerweb.servicios.impl.VehiculoDbDAO;

@WebListener
public class AppContextListener implements ServletContextListener  {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("Contexto de aplicacion inicializado");
		
		// Recupero datos de la base de datos de los parámetros iniciales
		ServletContext ctx = event.getServletContext();
		String bdUrl = ctx.getInitParameter("bdUrl");
		String bdUser = ctx.getInitParameter("bdUser");
		String bdPwd = ctx.getInitParameter("bdPwd");
		
		DatabaseManager db = new DatabaseManager(bdUrl, bdUser, bdPwd);
		Connection conn = db.obtenerConexion();
		ctx.setAttribute("db", db);
		
		// Guardamos en el contexto los objetos DAO
		IUsuarioDAO usuarios = new UsuarioDbDAO(conn);
		IClienteDAO clientes = new ClienteDbDAO(conn);
		IVehiculoDAO vehiculos = new VehiculoDbDAO(conn);
		IMecanicoDAO mecanicos = new MecanicoDbDAO(conn);
		IRepuestoDAO repuestos = new RepuestoDbDAO(conn);
		IHojaParteDAO partes = new HojaParteDbDAO(conn);
		IReparacionDAO reparaciones = new ReparacionDbDAO(conn);
		IFacturaDAO facturas = new FacturaDbDAO(conn, partes, repuestos, vehiculos, clientes, reparaciones);
		
		ctx.setAttribute("usuariosDAO", usuarios);
		ctx.setAttribute("clientesDAO", clientes);
		ctx.setAttribute("vehiculosDAO", vehiculos);
		ctx.setAttribute("mecanicosDAO", mecanicos);
		ctx.setAttribute("respuestosDAO", repuestos);
		ctx.setAttribute("partesDAO", partes);
		ctx.setAttribute("reparacionesDAO", reparaciones);
		ctx.setAttribute("facturasDAO", facturas);
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		System.out.println("Contexto de aplicacion inicializado");
		ServletContext ctx = event.getServletContext();
		DatabaseManager db = (DatabaseManager) ctx.getAttribute("db");
		db.cerrarConexion();
	}

}
