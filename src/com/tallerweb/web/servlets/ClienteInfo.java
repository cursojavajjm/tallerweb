package com.tallerweb.web.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tallerweb.modelos.Cliente;
import com.tallerweb.modelos.Vehiculo;
import com.tallerweb.servicios.IClienteDAO;
import com.tallerweb.servicios.IVehiculoDAO;

/**
 * Servlet implementation class Cliente
 */
@WebServlet(name = "cliente", urlPatterns = { "/cliente" })
public class ClienteInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClienteInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dni = request.getParameter("dni");
		
		ServletContext ctx = getServletContext();
		IClienteDAO clienteDAO = (IClienteDAO) ctx.getAttribute("clientesDAO");
		IVehiculoDAO vehiculoDAO = (IVehiculoDAO) ctx.getAttribute("vehiculosDAO");
	
		Cliente cliente = clienteDAO.recuperar(dni);
		Collection<Vehiculo> vehiculos = vehiculoDAO.recuperarPorCliente(dni);
		
		request.setAttribute("cliente", cliente);
		request.setAttribute("vehiculos", vehiculos);
		
		request.getRequestDispatcher("/cliente.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
