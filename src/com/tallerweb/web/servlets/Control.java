package com.tallerweb.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tallerweb.modelos.Usuario;
import com.tallerweb.servicios.IUsuarioDAO;

/**
 * Servlet implementation class Control
 */
@WebServlet(name = "control", urlPatterns = { "/control" })
public class Control extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Control() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("usuario");
		String password = request.getParameter("password");
		
		// Obtengo del contexto el objeto DAO para usuarios
		ServletContext ctx = getServletContext();
		IUsuarioDAO usuariosDAO = (IUsuarioDAO) ctx.getAttribute("usuariosDAO");
		
		// Valido el usuario
		RequestDispatcher rd;
		Usuario usuario = usuariosDAO.recuperar(nombre, password);
		if (usuario == null) {
			// Si no existe, lo devuelvo a la p�gina de error
			rd = request.getRequestDispatcher("error.jsp");
		} else {
			// Compruebo si es Admin o Mecanico
			if (usuario.isAdmin())
				 rd = request.getRequestDispatcher("/clientes");
			else rd = request.getRequestDispatcher("");
		}
		
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
