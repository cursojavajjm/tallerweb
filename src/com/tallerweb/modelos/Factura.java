package com.tallerweb.modelos;

import java.util.concurrent.atomic.AtomicInteger;

public class Factura {
	
	private static AtomicInteger idGen = new AtomicInteger(0);
	
	private int id;
	private String idCliente;
	private int idHojaParte;
	private int precio;
	
	public Factura(String dni, int hoja) {
		this.id = idGen.incrementAndGet();
		this.idCliente = dni;
		this.idHojaParte = hoja;
		this.precio = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public int getIdHojaParte() {
		return idHojaParte;
	}

	public void setIdHojaParte(int idHojaParte) {
		this.idHojaParte = idHojaParte;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Factura [id=" + id + ", idCliente=" + idCliente + ", idHojaParte=" + idHojaParte + ", precio=" + precio
				+ "]";
	}
	
}
