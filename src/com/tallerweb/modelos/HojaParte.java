package com.tallerweb.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class HojaParte {
	
	private static AtomicInteger idGen = new AtomicInteger(0);
	
	private int id;
	private String matricula;
	private int mecanico;
	private String evaluacion;
	private Date fechaEntrada;
	private List<Reparacion> reparaciones;
	
	public HojaParte(int id, String matricula, int mecanico, String evaluacion) {
		super();
		this.id = id;
		this.matricula = matricula;
		this.mecanico = mecanico;
		this.evaluacion = evaluacion;
		this.fechaEntrada = new Date();
		this.reparaciones = new ArrayList<Reparacion>();
	}
	
	public HojaParte(String matricula, int mecanico, String evaluacion) {
		super();
		this.id = idGen.incrementAndGet();
		this.matricula = matricula;
		this.mecanico = mecanico;
		this.evaluacion = evaluacion;
		this.fechaEntrada = new Date();
		this.reparaciones = new ArrayList<Reparacion>();
	}
	
	public static AtomicInteger getIdGen() {
		return idGen;
	}

	public static void setIdGen(AtomicInteger idGen) {
		HojaParte.idGen = idGen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public int getMecanico() {
		return mecanico;
	}

	public void setMecanico(int mecanico) {
		this.mecanico = mecanico;
	}

	public String getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(String evaluacion) {
		this.evaluacion = evaluacion;
	}

	public Date getFechaEntrada() {
		return fechaEntrada;
	}

	public void setFechaEntrada(Date fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}

	public List<Reparacion> getReparaciones() {
		return reparaciones;
	}

	public void setReparaciones(List<Reparacion> reparaciones) {
		this.reparaciones = reparaciones;
	}

	@Override
	public String toString() {
		return "HojaParte [id=" + id + ", matricula=" + matricula + ", mecanico=" + mecanico + ", evaluacion="
				+ evaluacion + ", fechaEntrada=" + fechaEntrada + ", reparaciones=" + reparaciones + "]";
	}
	
	
	
}
